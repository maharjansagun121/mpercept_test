from django.forms import ModelForm
from django.forms import Form
from django import forms
from .models import Operations_request



class Operations_requestForm(forms.Form):
    
    class Meta:
        model= Operations_request
        fields = ['first_name']
