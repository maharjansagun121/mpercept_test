from django.contrib import admin

# Register your models here.


from.models import Operations_request,Leave_request,TeaCoffee_request,MeetingRoom_booking

admin.site.register(Operations_request)
admin.site.register(Leave_request)
admin.site.register(TeaCoffee_request)
admin.site.register(MeetingRoom_booking)
