# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2019-06-07 08:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('automation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Leave_request',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('department', models.CharField(max_length=10)),
                ('leave_type', models.CharField(max_length=50)),
                ('reason', models.CharField(max_length=200)),
                ('leave_start_date', models.DateTimeField()),
                ('leave_end_date', models.DateTimeField()),
            ],
        ),
    ]
