from django.db import models

# Create your models here.


class Operations_request(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    department = models.CharField(max_length=10)
    operations_type = models.CharField(max_length=20)

    def __str__(self):
        return self.operations_type


class Leave_request(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    department = models.CharField(max_length=10)
    leave_type = models.CharField(max_length=50)
    reason = models.CharField(max_length=200)
    leave_start_date = models.DateTimeField()
    leave_end_date = models.DateTimeField()

    def __str__(self):
        return self.leave_type


class TeaCoffee_request(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    department = models.CharField(max_length=10)
    tea_with_sugar = models.BooleanField()
    tea_without_sugar = models.BooleanField()
    coffee_with_sugar = models.BooleanField()
    coffee_without_sugar = models.BooleanField()


    def __str__(self):
        return self.first_name

class MeetingRoom_booking(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    department = models.CharField(max_length=10)
    meeting_room = models.CharField(max_length=10)

    def __str__(self):
        return self.meeting_room
